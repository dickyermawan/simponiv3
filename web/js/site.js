/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-04-22 17:39:44
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-08 01:09:17
*/

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})