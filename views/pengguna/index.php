<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-01 22:07:36
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-15 22:40:14
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\icons\Icon;

use app\assets\ModalAsset;


ModalAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\PenggunaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengguna';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .modal-dialog {
        top: 7%;
    }
</style>

<div class="pengguna-index">

    <p>
        <?= Html::a(Icon::show('user-plus'). ' Tambah Pengguna', ['tambah'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
        'rowOptions' => function($model) {
            if ($model->id==2) return ['class' => 'info'];
            if ($model->id==1) return ['class' => 'info'];
        },
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'hak_akses',
            
            'nama_rs_puskesmas',
            'username',
            // 'kontak',
            // 'email:email',
            // 'alamat:ntext',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{leadView}<br/>{leadUpdate}<br/>{leadDelete}',
                'visibleButtons'=>[
                    'leadDelete'=> function($model){
                          return $model->id!=1 && $model->id!=2;
                     },
                ],
                'buttons'  => [
                    // 'leadView'   => function ($url, $model) {
                    //     $url = Url::to(['pengguna/view', 'id' => $model->id]);
                    //     return Html::a('Lihat', $url, ['title' => 'view', 'class' => 'btn btn-info btn-xs', 'style' => 'width:100%']);
                    // },
                    'leadView'   => function ($url, $model) {
                        return Html::button(
                            'Lihat',
                            [
                                // 'url' => '',
                                'title' => $model->nama_rs_puskesmas .' - level('. $model->hak_akses.')',
                                'value' => Url::to(['pengguna/detail','id'=>$model->id]),
                                'header' => 'Ubah Password',
                                'class' => 'modalButton btn btn-info btn-xs',
                                'style' => 'width:100%'
                            ]
                        );
                    },
                    'leadUpdate' => function ($url, $model) {
                        $url = Url::to(['pengguna/update', 'id' => $model->id]);
                        return Html::a('Ubah', $url, ['title' => 'update', 'class' => 'btn btn-success btn-xs', 'style' => 'width:100%']);
                    },
                    'leadDelete' => function ($url, $model) {
                        $url = Url::to(['pengguna/delete', 'id' => $model->id]);
                        return Html::a('Hapus', $url, [
                            'title'        => 'delete',
                            'data-confirm' => Yii::t('yii', 'Yakin mau hapus data ini?'),
                            'data-method'  => 'post',
                            'class' => 'btn btn-danger btn-xs',
                            'style' => 'width:100%'
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php  //js code: 
    $this->registerJs(
    "$(document).on('ready pjax:success', function() {
            $('.modalButton').click(function(e){
            e.preventDefault(); //for prevent default behavior of <a> tag.
            $('#modal').modal('show');  
        });
        });
    ")
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => [
        'id' => 'modalHeader'
    ],
    // 'header' => 'Ubah Profil',
    'id' => 'modal',
    'size' => 'modal-lg',
    // 'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>
