<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-01 22:07:36
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-28 11:27:25
*/

use yii\helpers\Html;
use yii\helpers\Url;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pengguna */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengguna-form">

<div class="panel panel-primary">
  <div class="panel-heading">Data Pengguna</div>
  <div class="panel-body">
    <?php 
        $form = ActiveForm::begin([
            // 'id' => 'login-form-horizontal', 
            'id' => $model->formName(), 
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::toRoute('pengguna/validasi')
        ]); 

        $hakAkses = [
            // 'admin' => 'ADMIN',
            'user' => 'USER'
        ];
    ?>

    <?php 
        if($model->hak_akses=='super_admin' || $model->hak_akses=='admin'){
            Html::activeHiddenInput($model, 'hak_akses', ['value' => $model->hak_akses]);
        }else{
            echo $form->field($model, 'hak_akses')->radioButtonGroup($hakAkses, [
                'class' => 'btn-group-md',
                'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
            ]);
        }
    ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_rs_puskesmas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kontak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

  
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
            <?php // Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>

</div>
