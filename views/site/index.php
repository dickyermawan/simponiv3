<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-04-22 14:28:21
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-13 12:48:14
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Daftar Rumah Sakit & Puskesmas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if ($model->id==2) return ['class' => 'info'];
        },
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'nama_rs_puskesmas',
            'alamat:ntext',
            'email:email',
            'kontak',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
