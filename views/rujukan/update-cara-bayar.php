<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-13 15:08:48
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-13 15:15:23
*/


if($id=='BPJS' || $id=='JKD'){
	$view = '<div id="update-form-cara-bayar">
                    <div class="form-group field-rujukan-nik">
<label class="control-label col-sm-3" for="rujukan-nik">NIK</label>
<div class="col-sm-9">
<input type="text" id="rujukan-nik" class="form-control" name="Rujukan[nik]">

<div class="help-block"></div>
</div>
</div>
                    <div class="form-group field-rujukan-no_bpjs_jkd">
<label class="control-label col-sm-3" for="rujukan-no_bpjs_jkd">No BPJS/JKD</label>
<div class="col-sm-9">
<input type="text" id="rujukan-no_bpjs_jkd" class="form-control" name="Rujukan[no_bpjs_jkd]">

<div class="help-block"></div>
</div>
</div>                    
                </div>';
}else{
	$view = '<div id="update-form-cara-bayar">
                    <input type="hidden" id="rujukan-nik" name="Rujukan[nik]">                    <input type="hidden" id="rujukan-no_bpjs_jkd" name="Rujukan[no_bpjs_jkd]">
                    
                                        
                </div>';
}

echo $view;